# Face-Detection
Collection of face detection models on Opencv, dlib, and Tensorflow

## Requirement
```bash
pip3 install numpy scipy tensorflow==1.4 dlib opencv-contrib-python
```

## Models
1. dlib
2. haar-cascade
3. Multi-task CNN (MTCNN)
4. Mobilenet-SSD
5. Tiny faces

## How-to-run on live camera
1. go to any model folder
2. run python 3 live_*.py
```bash
python3 live_*.py
```

## How-to-run on any static image
1. go to any model folder
2. paste the picture you want
3. open detect.py
```python
# replace with your picture name
img = imread('selfie.jpg')
```

## Results on static image

#### dlib library

![alt text](dlib/processed_selfie.jpg)

#### Haar-cascade opencv2

![alt text](haarcascade/processed_selfie.jpg)

#### mobilenet-SSD

![alt text](mobilenet-SSD/processed_selfie.jpg)

#### MTCNN

![alt text](MTCNN/processed_selfie.jpg)

#### Tiny faces

![alt text](tiny-faces/processed_selfie.jpg)

Docker image incoming.
