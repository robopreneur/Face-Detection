import visualization_utils
import cv2
import time
from scipy.misc import imread, imsave

detector = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')
min_size = (20,20)
haar_scale = 1.1
min_neighbors = 3
haar_flags = 0

img = imread('selfie.jpg')
input_img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
input_img = cv2.cvtColor(input_img, cv2.COLOR_BGR2GRAY)
detected = detector.detectMultiScale(input_img, haar_scale, min_neighbors, haar_flags, min_size)
for (x,y,w,h) in detected:
    visualization_utils.draw_bounding_box_on_image_array(img,y,x,y+h,x+w,'YellowGreen',display_str_list=['face'],use_normalized_coordinates=False)
imsave('processed_selfie.jpg',img)
