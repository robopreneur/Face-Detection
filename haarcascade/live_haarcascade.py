import visualization_utils
import cv2
import time

detector = cv2.CascadeClassifier('haarcascade_frontalface_alt.xml')
cap = cv2.VideoCapture(0)
min_size = (20,20)
haar_scale = 1.1
min_neighbors = 3
haar_flags = 0

while True:
    last_time = time.time()
    ret, img = cap.read()
    input_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    detected = detector.detectMultiScale(input_img, haar_scale, min_neighbors, haar_flags, min_size)
    for (x,y,w,h) in detected:
        visualization_utils.draw_bounding_box_on_image_array(img,y,x,y+h,x+w,'YellowGreen',display_str_list=['face'],use_normalized_coordinates=False)
    cv2.putText(img,'%.1f FPS'%(1/(time.time() - last_time)), (0,20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, 255)
    cv2.imshow("result", img)
    key = cv2.waitKey(1)
    if key == 27:
        break
