import tensorflow as tf
import tiny_face_model
import util
import visualization_utils
import time
import numpy as np
import pylab as pl
import cv2
import pickle
from scipy.special import expit

MAX_INPUT_DIM = 5000.0
prob_thresh = 0.4
line_width = 3
nms_thresh = 0.1

sess = tf.InteractiveSession()
weight_file_path = 'mat2tf.pkl'
x = tf.placeholder(tf.float32, [1, None, None, 3])
model = tiny_face_model.Model(weight_file_path)
score_final = model.tiny_face(x)
with open(weight_file_path, "rb") as f:
    _, mat_params_dict = pickle.load(f)
average_image = model.get_data_by_key("average_image")
clusters = model.get_data_by_key("clusters")
clusters_h = clusters[:, 3] - clusters[:, 1] + 1
clusters_w = clusters[:, 2] - clusters[:, 0] + 1
normal_idx = np.where(clusters[:, 4] == 1)
sess.run(tf.global_variables_initializer())

def overlay_bounding_boxes(raw_img, refined_bboxes, lw):
    for r in refined_bboxes:
        _score = expit(r[4])
        cm_idx = int(np.ceil(_score * 255))
        rect_color = [int(np.ceil(x * 255)) for x in util.cm_data[cm_idx]]
        _lw = lw
        if lw == 0:
            bw, bh = r[2] - r[0] + 1, r[3] - r[0] + 1
            _lw = 1 if min(bw, bh) <= 20 else max(2, min(3, min(bh / 20, bw / 20)))
            _lw = int(np.ceil(_lw * _score))

        _r = [int(x) for x in r[:4]]
        visualization_utils.draw_bounding_box_on_image_array(img,_r[1],_r[0],_r[3],_r[2],
                                                             'YellowGreen',display_str_list=['face'],use_normalized_coordinates=False)

def _calc_scales(raw_img):
    raw_h, raw_w = raw_img.shape[0], raw_img.shape[1]
    min_scale = min(np.floor(np.log2(np.max(clusters_w[normal_idx] / raw_w))),
                        np.floor(np.log2(np.max(clusters_h[normal_idx] / raw_h))))
    max_scale = min(1.0, -np.log2(max(raw_h, raw_w) / MAX_INPUT_DIM))
    scales_down = pl.frange(min_scale, 0, 1.)
    scales_up = pl.frange(0.5, max_scale, 0.5)
    scales_pow = np.hstack((scales_down, scales_up))
    scales = np.power(2.0, scales_pow)
    return scales

def _calc_bounding_boxes(score_cls_tf, prob_cls_tf,score_reg_tf, s):
    _, fy, fx, fc = np.where(prob_cls_tf > prob_thresh)
    cy = fy * 8 - 1
    cx = fx * 8 - 1
    ch = clusters[fc, 3] - clusters[fc, 1] + 1
    cw = clusters[fc, 2] - clusters[fc, 0] + 1
    Nt = clusters.shape[0]
    tx = score_reg_tf[0, :, :, 0:Nt]
    ty = score_reg_tf[0, :, :, Nt:2*Nt]
    tw = score_reg_tf[0, :, :, 2*Nt:3*Nt]
    th = score_reg_tf[0, :, :, 3*Nt:4*Nt]

    dcx = cw * tx[fy, fx, fc]
    dcy = ch * ty[fy, fx, fc]
    rcx = cx + dcx
    rcy = cy + dcy
    rcw = cw * np.exp(tw[fy, fx, fc])
    rch = ch * np.exp(th[fy, fx, fc])

    scores = score_cls_tf[0, fy, fx, fc]
    tmp_bboxes = np.vstack((rcx - rcw / 2, rcy - rch / 2, rcx + rcw / 2, rcy + rch / 2))
    tmp_bboxes = np.vstack((tmp_bboxes / s, scores))
    tmp_bboxes = tmp_bboxes.transpose()
    return tmp_bboxes

def evaluate(input_img):
    raw_img = cv2.cvtColor(input_img, cv2.COLOR_BGR2RGB)
    raw_img_f = raw_img.astype(np.float32)
    scales = _calc_scales(raw_img)
    bboxes = np.empty(shape=(0, 5))
    for s in scales:
        img = cv2.resize(raw_img_f, (0, 0), fx=s, fy=s, interpolation=cv2.INTER_LINEAR)
        img = img - average_image
        img = img[np.newaxis, :]
        tids = list(range(4, 12)) + ([] if s <= 1.0 else list(range(18, 25)))
        ignoredTids = list(set(range(0, clusters.shape[0])) - set(tids))
        score_final_tf = sess.run(score_final, feed_dict={x: img})
        score_cls_tf, score_reg_tf = score_final_tf[:, :, :, :25], score_final_tf[:, :, :, 25:125]
        prob_cls_tf = expit(score_cls_tf)
        prob_cls_tf[0, :, :, ignoredTids] = 0.0
        tmp_bboxes = _calc_bounding_boxes(score_cls_tf,prob_cls_tf,score_reg_tf, s)
        bboxes = np.vstack((bboxes, tmp_bboxes))
    refind_idx = tf.image.non_max_suppression(tf.convert_to_tensor(bboxes[:, :4], dtype=tf.float32),
                                              tf.convert_to_tensor(bboxes[:, 4], dtype=tf.float32),
                                              max_output_size=bboxes.shape[0], iou_threshold=nms_thresh)
    refind_idx = sess.run(refind_idx)
    refined_bboxes = bboxes[refind_idx]
    overlay_bounding_boxes(input_img, refined_bboxes, line_width)

cap = cv2.VideoCapture(0)
while True:
    last_time = time.time()
    ret, img = cap.read()
    evaluate(img)
    cv2.putText(img,'%.1f FPS'%(1/(time.time() - last_time)), (0,20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, 255)
    cv2.imshow("result", img)
    key = cv2.waitKey(1)
    if key == 27:
        break
