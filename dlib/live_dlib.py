import visualization_utils
import cv2
import dlib
import time

detector = dlib.get_frontal_face_detector()
cap = cv2.VideoCapture(0)
while True:
    last_time = time.time()
    ret, img = cap.read()
    input_img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    detected = detector(input_img, 1)
    for i, d in enumerate(detected):
        x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()
        visualization_utils.draw_bounding_box_on_image_array(img,y1,x1,y2,x2,'YellowGreen',display_str_list=['face'],use_normalized_coordinates=False)
    cv2.putText(img,'%.1f FPS'%(1/(time.time() - last_time)), (0,20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, 255)
    cv2.imshow("result", img)
    key = cv2.waitKey(1)
    if key == 27:
        break
