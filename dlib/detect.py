import visualization_utils
import cv2
import dlib
import time
from scipy.misc import imread, imsave

detector = dlib.get_frontal_face_detector()
img = imread('selfie.jpg')
detected = detector(img, 1)
for i, d in enumerate(detected):
    x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()
    visualization_utils.draw_bounding_box_on_image_array(img,y1,x1,y2,x2,'YellowGreen',display_str_list=['face'],use_normalized_coordinates=False)
imsave('processed_selfie.jpg',img)
